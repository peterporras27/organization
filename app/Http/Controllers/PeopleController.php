<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Person;

class PeopleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'People',
        );
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perpage = 10;

        if ( $request->input('perpage') ) {
            $perpage = preg_replace('/\D/', '', $request->input('perpage'));
            $perpage = !empty($perpage) ? $perpage : 50;   
        }

        $this->params['data'] = Person::orderBy('id', 'asc')->with('organizations')->paginate($perpage);
        $this->params['perpage'] = $perpage;
        $this->params['error'] = false;
        $this->params['message'] = 'success';

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'nullable|string',
            'phone' => 'nullable|string',
            'age' => 'nullable|integer',
            'address' => 'nullable|string',
            'organizations' => 'nullable|array',
        ]);

        $person = new Person();
        $person->fill($request->all());
        $person->save();

        $this->params['error'] = false;
        $this->params['message'] = 'success';

        if ( is_array( $request->input('organizations') ) ) 
        {
            $orgs = Organization::whereIn('id',$request->input('organizations'))->get();

            if ($orgs) 
            {
                foreach ($orgs as $org) 
                {
                    $org->people()->attach($person);
                }
            }
        }
    
        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $person = Person::find($id);
        
        if ($person) {
            $this->params['error'] = false;
            $this->params['data'] = $person->with('organizations')->first();
        }

        return response()->json($this->params);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $person = Person::find($id);

        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'nullable|string',
            'phone' => 'nullable|string',
            'age' => 'nullable|integer',
            'address' => 'nullable|string',
            'organizations' => 'nullable|array',
        ]);

        $person->fill($request->all());
        $person->save();

        $this->params['error'] = false;
        $this->params['message'] = 'success';
        
        if ( is_array( $request->input('organizations') ) ) 
        {
            $orgs = Organization::whereIn('id',$request->input('organizations'))->get();

            $this->params['orgs'] = $orgs;
            $this->params['organizations'] = $request->input('organizations');

            if ($orgs) 
            {
                $person->organizations()->detach();
                foreach ($orgs as $org) 
                {
                    $person->organizations()->attach($org);
                }    
            }
        }

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $person = Person::find( $id );

        if ( $person )
        {
            $person->delete();
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }
}
