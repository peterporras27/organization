<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Organization;

class OrganizationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);

        $this->params = array(
            'error' => true,
            'message' => 'Please try again.',
            'title' => 'Organization',
        );
    }

    public function getorgs()
    {
        return Organization::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perpage = 10;

        if ( $request->input('perpage') ) {
            $perpage = preg_replace('/\D/', '', $request->input('perpage'));
            $perpage = !empty($perpage) ? $perpage : 50;   
        }

        $this->params['data'] = Organization::orderBy('id', 'asc')->paginate($perpage);
        $this->params['perpage'] = $perpage;
        $this->params['error'] = false;
        $this->params['message'] = 'success';

        return response()->json($this->params); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'nullable|string',
        ]);

        $organization = new Organization();
        $organization->fill($request->all());
        $organization->save();

        $this->params['error'] = false;
        $this->params['message'] = 'success';
    
        return response()->json($this->params); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $org = Organization::find($id);
        
        if ($org) {
            $this->params['error'] = false;
            $this->params['data'] = $org->with('people')->first();
        }

        return response()->json($this->params);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $org = Organization::find($id);

        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'nullable|string',
        ]);

        $org->fill($request->all());
        $org->save();

        $this->params['error'] = false;
        $this->params['message'] = 'success';

        return response()->json($this->params); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $org = Organization::find( $id );

        if ( $org )
        {
            $org->delete();
            $this->params['error'] = false;
            $this->params['message'] = 'success';
        }

        return response()->json($this->params); 
    }
}
