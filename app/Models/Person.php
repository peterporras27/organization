<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'age',
        'phone',
        'address',
    ];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class);
    }
}
