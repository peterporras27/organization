function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', redirect: { name: 'login' } },
  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },
  { path: '/home', name: 'home', component: page('home.vue') },
  { path: '/people', name: 'people', component: page('people/index.vue'),
    children: [
      { path: '', redirect: { name: 'people.list' } },
      { path: 'list', name: 'people.list', component: page('people/list.vue') },
      { path: 'create', name: 'people.create', component: page('people/create.vue') },
      { path: 'details/:id', name: 'people.details', component: page('people/details.vue') }
    ]
  },
  { path: '/organizations', component: page('organizations/index.vue'),
    children: [
      { path: '', redirect: { name: 'organizations.list' } },
      { path: 'list', name: 'organizations.list', component: page('organizations/list.vue') },
      { path: 'create', name: 'organizations.create', component: page('organizations/create.vue') },
      { path: 'details/:id', name: 'organizations.details', component: page('organizations/details.vue') }
    ]
  },
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] },

  { path: '*', component: page('errors/404.vue') }
]
