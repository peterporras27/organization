# INSTALATION #

### SETUP ###

Create a database and name it to:
```
organization
```

Copy .env.example file to .env file

### USAGE ###

Open terminal and go to the project folder
run these commands:

```
composer update
```
```
npm install
```

#### DEVELOPMENT
```bash
# build and watch
npm run watch

# serve with hot reloading
npm run hot
```

```bash
php artisan migrate --seed
```

#### PRODUCTION

```bash
npm run production
```

```bash
php artisan serve
```

Admin Login:
```bash
email: admin@admin.com
password: secret
```