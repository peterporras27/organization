<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'admin@admin.com',
            'password' => Hash::make('secret'),
        ]);

         DB::table('organizations')->insert([
            'name' => 'Akatsuki',
            'description' => 'is a criminal organization of S-Class Criminals and Missing-nin and is the most wantyed group in all of shinobi world.',
        ]);
    }
}
